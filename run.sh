set -e -x

ARGS="$ARGS -fforce-recomp"
ARGS="$ARGS -ddump-to-file"
ARGS="$ARGS -ddump-inlinings"
ARGS="$ARGS -ddump-rule-firings"
ARGS="$ARGS -ddump-simpl"
ARGS="$ARGS -dverbose-core2core"
ARGS="$ARGS -dsuppress-idinfo"
ARGS="$ARGS -dsuppress-coercions"
ARGS="$ARGS -dsuppress-uniques"
#ARGS="$ARGS -dsuppress-coercions"

get_ghc() {
  local version="$1"
  local exp="nixpkgs.haskell.compiler.ghc$version"
  nix build --quiet $exp
  echo $(nix eval --raw $exp)/bin/ghc
}

build() {
  local version="$1"
  shift
  ghc=$(get_ghc "$version")
  cabal v2-build --write-ghc-environment-file=always -w $ghc
  out="$(pwd)/out-$version"
  rm -Rf $out
  mkdir -p $out
  $ghc -O2 -isrc -iapp Main -dumpdir $out $ARGS >& "$out/log"
  /opt/exp/ghc/ghc-utils/split-core2core.py $(find $out -iname "*.verbose-core2core")
}

build 884
build 8102
